<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RWAS/YbZ<t5-=<NXvmj|fo{*57Jdv|2{j9(@qJF}Y;gTcqQr P.ZANqD`1kf]/z}' );
define( 'SECURE_AUTH_KEY',  'N%:[>][TLHSjxla:qMGck%AWF|`Bw@;&fV{`MD3fH9;Svzx3}3>F]]k:6JI;-/s(' );
define( 'LOGGED_IN_KEY',    'GCNo~Zo~W,3+[M#aN}As s}*JA38>$3)^bZ{HA4_78mUUydnBsz/{.O:Y4-b-xl<' );
define( 'NONCE_KEY',        'W{ZuCmhn%5?>ffaRKwaa4H}J4z(.8I4*5F!Yu#}D{9f $SKo|(MxoP:Uk7Kp#a6k' );
define( 'AUTH_SALT',        'tl$DCUfCV,K;rvCR!$4}f.MxQ,vX-cHJ]Rp[^l19zBo+t`*d;:,E c;z#Y*S@%bD' );
define( 'SECURE_AUTH_SALT', '/hgmqQ8}ynMt+>[)d0Au0&PVi14FtGKO@oGgMUflD`r8~`d[5(/ j.i/+gZ&R=d{' );
define( 'LOGGED_IN_SALT',   'fsSxn6a8?p}qH+k}Kj%:@UO8)uH.m!;d!{eXBNkwtphEeKI#`sCPiA<>Ip>eni&2' );
define( 'NONCE_SALT',       ':A6?CW&X]bq)8O3SL}#W!^RS=Qo5=e|3G%JS?O-.b$coHEs-TG6{Vno2g8e9i5Ai' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
