<?php
/*
Plugin Name: Test task custom plugin for example.com
Description: Test task code changes for example.com
*/
/*code for test task*/
/*1) Потрібно створити CPT(Custom Post Type) з назвою "Speakers".*/
function create_custom_post_type(){
	$labels = array(
		'name' => __( 'Speakers', 'speaker' ),
		'singular_name' => __( 'Speaker', 'speaker' ),
		'add_new' => __( 'New Speaker', 'speaker' ),
		'add_new_item' => __( 'Add New Speaker', 'speaker' ),
		'edit_item' => __( 'Edit Speaker', 'speaker' ),
		'new_item' => __( 'New Speaker', 'speaker' ),
		'view_item' => __( 'View Speakers', 'speaker' ),
		'search_items' => __( 'Search Speakers', 'speaker' ),
		'not_found' =>  __( 'No Speakers Found', 'speaker' ),
		'not_found_in_trash' => __( 'No Speakers found in Trash', 'speaker' ),
	);
	$args_supports = array(
		'title',
		'editor',		 
		'thumbnail',		 
	);
	$args = array(
		'labels' => $labels,
		'has_archive' => true,
		'public' => true,
		'hierarchical' => false,
		'supports' => $args_supports,
		'taxonomies' => 'category',
		'rewrite'   => array( 'slug' => 'speaker' ),
		'show_in_rest' => true
	   );	
	register_post_type('speaker', $args);	
	flush_rewrite_rules();
}
add_action('init', 'create_custom_post_type');
/*2) Задати 2 таксономії з назвами "Positions", "Countries" для CPT "Speakers"*/
function create_custom_taxonomies(){
	register_taxonomy('Positions', 'speaker', array(    
		'hierarchical' => false,    
		'labels' => array(
		  'name' => _x( 'Positions', 'taxonomy general name' ),
		  'singular_name' => _x( 'Position', 'taxonomy singular name' ),      
		  'all_items' => __( 'All Positions' ),      
		  'edit_item' => __( 'Edit Position' ),
		  'update_item' => __( 'Update Position' ),
		  'add_new_item' => __( 'Add New Position' ),
		  'new_item_name' => __( 'New Position Name' ),
		  'menu_name' => __( 'Positions' ),
		),
	  ));
	  register_taxonomy('Countries', 'speaker', array(    
		'hierarchical' => false,    
		'labels' => array(
		  'name' => _x( 'Countries', 'taxonomy general name' ),
		  'singular_name' => _x( 'Country', 'taxonomy singular name' ),      
		  'all_items' => __( 'All Countries' ),      
		  'edit_item' => __( 'Edit Country' ),
		  'update_item' => __( 'Update Country' ),
		  'add_new_item' => __( 'Add New Country' ),
		  'new_item_name' => __( 'New Country Name' ),
		  'menu_name' => __( 'Countries' ),
		),
	  ));
}
add_action('init', 'create_custom_taxonomies');
