(function ($) {
    //AJAX      
    let eventRequest;
    $(document.body).on('change', '.custom_filter select', function(e){
        e.preventDefault();
        let url = $(this).val();
        //console.log(url);
        //console.log($(this));
        //$('#wp--skip-link--target').addClass('ajax-on');
        $(document.body).trigger('custom_filter_ajax_request', url);
    });
    /*$(document.body).on('click', '.custom_filter a', function(e){
        e.preventDefault();
        let url = $(this).attr('href');
        //$('#wp--skip-link--target').addClass('ajax-on');
        $(document.body).trigger('custom_filter_ajax_request', url);
    });  */
    $(document.body).on('custom_filter_ajax_request', function(e, url, element){
        //console.log(url);
        let $content = $('main');
        if(url.slice(-1)=="?"){
            url = url.slice(0, -1);
        }
        url = url.replace(/%2C/, ",");
        //console.log(url);
        if(eventRequest){
            eventRequest.abort();
        }
        $.get(url, function(res){
            //console.log(typeof res);
            //let toRes = res.startsWith("main");
            //console.log(toRes);
            //console.log(res);
            console.log($(res).find("#wp--skip-link--target"));
            //$content.replaceWith($(res).find("primary"));
            //wp--skip-link--target
            $content.replaceWith($(res).find("main"));
            $('main').removeClass('alignwide');
        }, 'html');
    });
})(jQuery)