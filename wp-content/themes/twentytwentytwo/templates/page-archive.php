<?php
/* 
Template Name: Archives
*/
get_header(); 
?>
<style>    
    .content-wrap{
        display: flex;
    }
    #primary{
        display: flex; 
    }
    #content{
        display: flex;   
        flex-wrap: wrap;
        border: solid red 2px;     
        width: 1600px;
        padding-left: 15px;
    }
    .whole_speaker{
        flex: 0 0 25%;
        max-width: 25%;
        margin-bottom: 5px;
    }
    .speaker_title{
        margin-bottom: -15px;
    }
    .whole_speaker div p img{
        width: 150px;
        height: 150px;
    }
    .custom_filter{
        /*display: none;*/
        width: 200px;
        border: solid red 2px;
        color: green;
        font-weight: bold;
    }
    .custom_dropdown_filter{
        display: none;
    }
    .tax_list{
        list-style: none;
    }
    .tax_item{        
        font-weight: normal;        
    }
    main{
        display: flex;
    }
    .wp-block-post-template{
        display: flex;
        flex-wrap: wrap;
        border: solid red 2px;
        /*width: 1600px;*/
        padding-left: 15px;
    }
    .speaker{
        flex: 0 0 25%;
        max-width: 20%;
        margin: 5px;
        height: 300px;
    }
    .speaker img{
        width: 150px;
        height: 150px;
    }
    h2{
        font-weight: normal !important;
        font-size: 15px !important;
    }
    ul, li{
        list-style: none;
    }
</style>
    <div id="wp--skip-link--target" class="content-wrap">
        <div class="custom_filter">
            <p>ajax sidebar filter</p>
		    <?php
		    $args = array(
			    'public'   => true,
			    '_builtin' => false
		    );
		    $output = 'objects';
		    $operator = 'and';
		    $taxonomies = get_taxonomies( $args, $output, $operator );
		    if( $taxonomies ){
			    foreach( $taxonomies as $taxonomy ){
				    echo '<div>' .
				         $taxonomy->name .
				         '</div>';
				    $all_terms = get_terms(array('taxonomy' => $taxonomy->name, 'hide_empty' => 0, 'parent' => '0' ) );
				    $tax_link = strtolower($taxonomy->name);
                    echo "<select name='$tax_link'>";
                    echo "<option value=''>All</option>";
                    foreach($all_terms as $term){
	                    echo "<option value='http://localhost/speaker/?$tax_link=$term->name'>$term->name</option>";
                    }
                    echo "</select>";
                    /*echo "<ul class='tax_list'>";
				    foreach($all_terms as $term){
					    $tax_link = strtolower($taxonomy->name);
					    //<a href='http://localhost/speaker/?$taxonomy->name=$term->name'>
                        echo "<li>
                                <a href='http://localhost/speaker/?$tax_link=$term->name'>
                                    <input type='radio' class='tax_item' name='$tax_link'>$term->name
                                </a>
                            </li>";
				    }
				    echo "</ul>";*/
			    }
		    }
		    ?>
        </div>
    <main id="content" role="main">
            <?php $loop = new WP_Query( array( 'post_type' => 'speaker', 'posts_per_page' => 8 ) ); 
            while ( $loop->have_posts() ) : $loop->the_post();
            ?>            
            <div class="whole_speaker">
                <div class="speaker_title">
                    <?php the_title(); ?>
                </div>            
                <div class="speaker_content">            
                    <?php the_content(); ?>                            
                    <?php the_shortlink('View'); ?>                    
                </div>
            </div>         
            <?php endwhile; ?>            
        </main><!-- #content -->
    </div><!-- #primary -->    
    <div class="custom_dropdown_filter">
        <?php 
            $args = array(
                'public'   => true,
                '_builtin' => false
            );
            $output = 'objects'; 
            $operator = 'and';         
            $taxonomies = get_taxonomies( $args, $output, $operator );  
            if( $taxonomies ){
                foreach( $taxonomies as $taxonomy ){
                    echo '<div>' . 
                        $taxonomy->name .
                        '</div>';      
                    if( $terms = get_terms( array( 'taxonomy' => $taxonomy->name, 'orderby' => 'name' ) ) ) {
                        echo '<select name="tax_filter"><option>Select '.$taxonomy->name.'...</option>';
                        foreach ( $terms as $term ) {
                            echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // в качестве value я взял ID рубрики
                        }
                        echo '</select>';
                    }
                }
            }
            /*if( $terms = get_terms( array( 'taxonomy' => 'category', 'orderby' => 'name' ) ) : // как я уже говорил, для простоты возьму рубрики category, но get_terms() позволяет работать с любой таксономией
                echo '<select name="categoryfilter"><option>Выберите категорию...</option>';
                foreach ( $terms as $term ) {
                    echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // в качестве value я взял ID рубрики
                }
                echo '</select>';
            endif;*/
        ?>
    </div>
    
<?php get_footer(); ?>